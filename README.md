## ChatApp
This is real-time chat app. It consists of two tiers: front-end and back-end.

### Frontend
* has a welcome screen which asks the user to enter his nickname to log into the app
* has a multi-room chat page, where the user can communicate with other people
* new rooms can be created as necessary; the users can post messages to any room

#### The chat page does:
* display the list of currently connected people in the chat
* display the history of messages in ascending order by datetime
* have user interface for writing and sending messages
* update the messages in real-time
* visually indicate messages authored by the current user

### The Backend
* persists messages to a permanent storage
* maintains user sessions (i.e. if user refreshes the browser, he doesn't have to reenter his nickname)

### How to launch

#### Pre-requisites
[Docker](https://www.docker.com) is required on the target machine in order to launch both the client app and the server. Also, a companion utility to `docker` called `docker-compose` should be installed to launch both tiers in a dockerized environment.

#### Run
1. Clone the [repo](https://gitlab.com/atymchuk/chatapp.git) to the target machine `git clone https://gitlab.com/atymchuk/chatapp.git`
2. Run `cd chatapp` to change the current directory
3. Run `docker-compose up -d`. This will set up three separate docker containers:
    * a mongodb server with a pre-seeded database
    * an API server exposing a socket connection to the client
    * an instance of nginx serving the app
4. Open http://localhost:3000 in your browser to run the app

### Test
0. As of now, the FE & BE tests can only be run when the mongodb container is up and running on the host machine.
1. To run back-end tests, cd to the `/server` folder and type `npm install && npm run build && npm test`
2. To run front-end test, cd to the `/client` folder and type `npm install && npm test`
3. The test results for both client and server can be found [here](https://gitlab.com/atymchuk/chatapp/blob/master/client/src/tests/chatapp-client-test-result.png) and [here](https://gitlab.com/atymchuk/chatapp/blob/master/server/test/chatapp-server-test-result.png)

NOTE: it's better to stop the `web` & `server` containers while running tests

#### Cleaning up
Since the `docker-compose up -d` command builds images and creates containers on the target machine, cleaning it all up would require the following steps:
* stop and remove the containers, volumes & possibly networks with `docker-compose down -v` command
* removing images with `docker rmi $(docker images -q | head -n 3)` command

---
### Notes
* For demo purposes the app comes with one chat room populated with sample messages from imaginary users. It does not allow to create more chat rooms though.
* No registration / authentication required, a user can just write his/her nickname and start chatting (come on, this is a starter!)
* The app has an extensible modular structure that allows to easily add more features to both tiers
    * the API is designed with versioning in mind
    * the BE has a modular structure, i.e. api, db models, socket
    * each of the BE parts can be configured, i.e. api, session, logger, etc.
    * the FE leverages reusable components
* both BE & FE have a test coverage with a few basic tests
* a further TODO list would include:
    * securing the database with a password
    * running tests in containers
    * creating a docker-compose.dev.yml for dev environment (with --watch on fs)
    * private person-to-person messages
