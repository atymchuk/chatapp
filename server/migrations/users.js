/**
 * Migration script for the Users model
 */

import User from '../src/models/User';

/**
 * Insert test users
 */
export function up() {
  // create a default public room
  // add more as needed
  const users = [
    { local: { username: 'larryking' } },
    { local: { username: 'johndoe' } },
  ];

  return User.insertMany(users);
}

/**
 * Remove all users
 */
export function down() {
  return User.deleteMany({});
}
