/**
 * Migration script for the Rooms model
 */

import Room from '../src/models/Room';

/**
 * Insert a default Public room
 */
export function up() {
  // create a default public room
  // add more as needed
  const rooms = [
    { name: 'Public chat', private: false },
  ];

  return Room.insertMany(rooms);
}

/**
 * Remove all rooms
 * Warning: if you don't run the down migration of the Messages model,
 * the messages will be orphaned
 */
export function down() {
  return Room.deleteMany({});
}
