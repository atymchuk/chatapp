/**
 * Migration for the Messages model
 */

import Room from '../src/models/Room';
import Message from '../src/models/Message';

/**
 * Insert sample messages
 */
export function up() {
  
  return Room.findOne({ name: 'Public chat' })
    .then(doc => doc.id)
    .then(roomId => {
      const messages = [
        'Hey there! How are you doing?',
        'Fine, thanks. How about going to the cinema tonight?',
        'Great idea! What is the title?',
        'The Wolf of Wall Street with Leo DiCaprio',
        'Wonderful! See you tonight then!',
        'Ciao!'
      ]
      .map((text, i) => {
        const userId = i % 2 === 0 ? 'johndoe' : 'larryking';
        // increments of 30 minutes
        const time = new Date(new Date().valueOf() - 2 * 24 * 60 * 60 * 1000 + 30 * 60 * 1000 * i);
        return { roomId, userId, time, text };
      });
    
      return Message.insertMany(messages);    
    });
}

/**
 * Remove all messages in all rooms
 */
export function down() {
  return Message.deleteMany({});
}
