import mongoose from 'mongoose';

import models from '../dist/models';

mongoose.connect('mongodb://localhost/chatapp-test');
const connection = mongoose.connection;
const { User, Message, Room } = models;

describe('Database Tests', () =>  {
  beforeAll(done => {
    connection.on('error', console.error.bind(console, 'connection error'));
    connection.once('open', done);
  });

  // After all tests are finished drop the database and close the connection
  afterAll(done => {
    connection.db.dropDatabase(() => {
      connection.close(done);
    });
  });

  describe('User Model Test', () => {
    expect.hasAssertions();

    it('should not pass validation if `local.username` is empty', (done) => {
      const user = new User({ local: { username: '' } });

      user.validate(err => {
        expect(err).toBeInstanceOf(Error);

        expect(err.errors['local.username'].name).toEqual('ValidatorError');

        expect(err.errors['local.username'].message).toEqual('Path `local.username` is required.');
        done();
      });
    });

    it('should save a new user', async () => {
      const { _doc: user } = await new User({ local: { username: 'johndoe' }}).save();

      expect(Object.keys(user)).toEqual(expect.arrayContaining(['local', 'facebook', '_id', '__v']));

      expect(user.local.username).toEqual('johndoe');
    });

    it('should find a user', async () => {
      const { _doc: user } = await User.findOne({ local: { username: 'johndoe' }});

      expect(Object.keys(user)).toEqual(expect.arrayContaining(['local', 'facebook', '_id', '__v']));

      expect(user.local.username).toEqual('johndoe');
    });

    it('should delete a user', async () => {
      const user = await User.deleteOne({ local: { username: 'johndoe' }});

      expect(Object.keys(user)).toEqual(expect.arrayContaining(['n', 'ok']));

      expect(user.n).toEqual(1);
    });
  });

  describe('Room Model Test', () => {
    expect.hasAssertions();

    it('should not pass validation if `private` is empty', (done) => {
      const room = new Room({ name: 'room' });

      room.validate(err => {
        expect(err).toBeInstanceOf(Error);

        expect(err.errors.private.name).toEqual('ValidatorError');

        expect(err.errors.private.message).toEqual('Path `private` is required.');
        done();
      });
    });

    it('should save a new room', async () => {
      const { _doc: room } = await new Room({ name: 'Public', private: false }).save();

      expect(Object.keys(room)).toEqual(expect.arrayContaining(['_id', 'name', 'private']));
      
      expect(room.name).toEqual('Public');
    });

    it('should find a room', async () => {
      const { _doc: room } = await Room.findOne({ name: 'Public' });

      expect(Object.keys(room)).toEqual(expect.arrayContaining(['_id', 'name', 'private']));

      expect(room.name).toEqual('Public');

      expect(room.private).toEqual(false);
    });

    it('should delete a room', async () => {
      const room = await Room.deleteOne({ name: 'Public' });

      expect(Object.keys(room)).toEqual(expect.arrayContaining(['n', 'ok']));

      expect(room.n).toEqual(1);
    });

    it('should insert multiple rooms', async () => {
      const rooms = [
        { name: 'Public', private: false },
        { name: 'Chat room', private: true },
      ];
      
      const result = await Room.insertMany(rooms);

      expect(result).toHaveLength(2);

      expect(result[0]).toMatchObject(rooms[0]);
    });
  });

  describe('Message Model Test', () => {
    expect.hasAssertions();

    const msg = { roomId: 'Public', userId: 'jerrylane', text: 'Hi Bob', time: new Date() };

    it('should not pass validation of empty message', (done) => {
      const message = new Message({});

      message.validate(err => {
        expect(err).toBeInstanceOf(Error);

        expect(err.errors.roomId.message).toEqual('Path `roomId` is required.');
        done();
      });
    });

    it('should save a new message', async () => {
      const { _doc: message } = await new Message(msg).save();

      expect(Object.keys(message)).toEqual(expect.arrayContaining(['_id', 'roomId', 'userId', 'text', 'time']));
      
      expect(message.text).toEqual('Hi Bob');
    });

    it('should find a message', async () => {
      const { _doc: message } = await Message.findOne({ time: msg.time });

      expect(Object.keys(message)).toEqual(expect.arrayContaining(['_id', 'roomId', 'userId', 'text', 'time']));

      expect(message.userId).toEqual('jerrylane');

      expect(message.time).toEqual(msg.time);
    });

    it('should delete a message', async () => {
      const result = await Message.deleteOne({ time: msg.time });

      expect(result).toMatchObject({ n: 1, ok: 1 });
    });

    it('should insert multiple messages', async () => {
      const messages = [
        { roomId: 'Public', userId: 'jerrylane', text: 'Hi John', time: new Date() },
        { roomId: 'Public', userId: 'johndoe', text: 'Hi Jerry', time: new Date() + 1000 },
      ];
      
      const result = await Message.insertMany(messages);

      expect(result).toHaveLength(2);

      expect(result[0]).toMatchObject(messages[0]);
    });

    it('should update multiple messages', async () => {      
      const result = await Message.updateMany({ roomId: 'Public' }, { roomId: 'Public Chat Room' });

      expect(result).toMatchObject({ n: 2, nModified: 2, ok: 1});
    });
  });
});
