import chai from 'chai';
import chaiHttp from 'chai-http';

import app, { server } from '../dist/index.js';

chai.use(chaiHttp);

describe('API', () => {
  afterAll((done) => {
    server.close(done);
  });

  describe('POST /api/v1/auth/login', () => {

    it('should log the user in', (done) => {
      chai.request(app)
      .post('/api/v1/auth/login')
      .send({ username: 'johndoe' })
      .end((err, res) => {
        // there should be no error
        expect(err).toEqual(null);
        // there should be a 200 status code
        expect(res.status).toEqual(200);
        // the response should be JSON
        expect(res.type).toEqual('application/json');
        // the body should be { data: 'johndoe' }
        expect(res.body.data).toEqual('johndoe');
        done();
      });
    });
  
    it('should not login if username is longer than 20 chars', (done) => {
      chai.request(app)
      .post('/api/v1/auth/login')
      .send({ username: 'hkjkjhkjhhgjhgjgfuyoit' })
      .end((err, res) => {
        expect(err).toEqual(null);
  
        expect(res.status).toEqual(400);
  
        expect(res.type).toEqual('application/json');
  
        expect(res.body.error).toEqual('Name too long');
        done();
      });
    });
  
    it('should not login if username is empty', (done) => {
      chai.request(app)
      .post('/api/v1/auth/login')
      .send({ username: '' })
      .end((err, res) => {
        expect(err).toEqual(null);
  
        expect(res.status).toEqual(401);
  
        expect(res.type).toEqual('application/json');
  
        expect(res.body.error).toEqual('Empty username');
        done();
      });
    });
  
  });
  
  describe('POST /api/v1/auth/ping', () => {
    let agent;
    beforeAll(() => {
      agent = chai.request.agent(app);
    });
  
    afterAll(() => {
      agent.close();
    });
  
    it('should not ping back OK an unauthenicated user', (done) => {
      chai.request(app)
      .get('/api/v1/auth/ping')
      .end((err, res) => {
        expect(err).toEqual(null);
        
        expect(res.status).toEqual(401);
  
        expect(res.type).toEqual('application/json');
  
        expect(res.body.error).toEqual('Not authenticated');
        done();
      });
    });
  
    it('should ping back OK an authenticated user', (done) => {
      agent
      .post('/api/v1/auth/login')
      .send({ username: 'johndoe' })
      .then(res => {
        
        expect(res.status).toEqual(200);
  
        return agent
          .get('/api/v1/auth/ping')
          .send()
          .then(res => {
  
            expect(res.status).toEqual(200);
            done();
          });
      })
      .catch(err => {
        console.error(err.message);
        done();
      });
    });
  
  });
  
  describe('GET /api/v1/auth/logout', () => {
    let agent;
    beforeAll(() => {
      agent = chai.request.agent(app);
    });
  
    afterAll(() => {
      agent.close();
    });
  
    it('should not logout an unauthenticated user', (done) => {
      chai.request(app)
      .get('/api/v1/auth/logout')
      .end((err, res) => {
        expect(res.status).toEqual(401);
  
        expect(res.body.error).toEqual('Not authenticated');
        
        expect(res.type).toEqual('application/json');
        done();
      });
    });
  
    it('should logout an authenticated user', (done) => {
      agent
      .post('/api/v1/auth/login')
      .send({ username: 'johndoe' })
      .then(res => {
  
        expect(res.status).toEqual(200);
  
        return agent
          .get('/api/v1/auth/logout')
          .query({ username: 'johndoe' })
          .send()
          .then(res => {
  
            expect(res.status).toEqual(200);
  
            expect(res.type).toEqual('application/json');
  
            expect(res.body.data).toEqual('Logged out');
            done();
          });
      })
      .catch(err => {
        console.error(err.message);
        done();
      });
    });
  
  });

});
