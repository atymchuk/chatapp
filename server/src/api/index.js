import { apiUrl } from '../config/api';
import auth from './auth';

export default function api(app) {
  app.use(`${apiUrl}/auth`, auth);
  // you can add more routes here
}
