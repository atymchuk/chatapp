import express from 'express';
import logger from '../config/logger';
const router = express.Router();

// use this as a route guard for preventing unauthorized access
export const authenticated = (req, res, next) => {
  if (req.session.username) {
    next();
  } else {
    res.status(401).json({ error: 'Not authenticated' });
  }
}

router.post('/login', (req, res) => {
  const username = req.body.username;

  if (!username) {
    return res.status(401).json({ error: 'Empty username' });
  }

  if (username && username.length > 20) {
    return res.status(400).json({ error: 'Name too long' });
  }

  // the user is alredy authenticated
  if (req.session.username === username) {
    return res.status(200).json({ data: username });
  }

  // ther user tries to authenticate
  if (!req.session.username && username) {

    const { User } = req.app.set('models');
    const query = { local: { username } };

    // we'll insert the user if she doesn't exit yet
    return User.findOneAndUpdate(query, { ...query }, { upsert: true })
      .then(() => {
        req.session.username = username;
        res.status(200).json({ data: username });
      })
      .catch(error => {
        logger.error(error);
        res.status(400).json({ error: 'Database error' });
      });
  }
  
  // unauthenticated, including the case of session.username !== username
  res.status(403).json({ error: 'Access forbidden' });
});

// serves as an authentication check, p.e. upon browser refresh
router.get('/ping', authenticated, (req, res) => {
  res.status(200).json({ data: 'User authenticated' });
});

router.get('/logout', authenticated, async (req, res) => {
  const { User } = req.app.set('models');
  const { username } = req.params;

  try {
    await User.update({ local: { username }}, { $set: { currentRoomId: null }});
  } catch (error) {
    logger.error(error);
  }

  req.session.destroy(err => {
    if (err) {
      res.status(500).json({ error: 'Logout error' });
    } else {
      res.status(200).json({ data: 'Logged out' });
    }
  })
});

export default router;
