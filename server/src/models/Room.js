import mongoose, { Schema } from 'mongoose';

const schema = Schema(
  { 
    name: { type: String, unique: true, required: true },
    private: { type: Boolean, required: true }
  }, 
  { 
    versionKey: false,
    toObject: {
      virtuals: true,
      transform: function(doc, ret) {
        const { _id, ...rest } = ret;
        return { ...rest };
      }
    }
  }
);

export default  mongoose.model('Room', schema);
