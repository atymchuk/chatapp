import mongoose, { Schema } from 'mongoose';

const schema = Schema(
  {
    roomId: { type: String, required: true },
    userId: { type: String, required: true },
    text: { type: String, required: true },
    time: { type: Date, required: true },
  },
  { 
    toObject: {
      virtuals: true,
      transform: function(doc, ret) {
        const { _id, __v, ...rest } = ret;
        return { ...rest };
      }
    }
  }
);

schema.index({ time: 1, type: -1 });

export default mongoose.model('Message', schema);
