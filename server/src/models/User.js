import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const schema = Schema({
  /**
   * A locally managed user
   * Note: for prod we'd need to implement the password and email as well
   */
  currentRoomId: String,
  local: {
    username: { type: String, unique: true, required: true },
    password: String,
    email: String,
  },
  /**
   * This is for a future extension, since it allows to properly structure
   * the User schema without the need to migrate it
   */
  facebook: {
    id: String,
    username: String,
    token: String,
    email: String,
  }
});

schema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(12), null);
};

schema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

export default mongoose.model('User', schema);
