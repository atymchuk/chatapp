export default {
  secret: 'chatapp.secret',
  name: 'chatapp.sid',
  proxy: true,
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
    // ttl in milliseconds, default - 1 day
    maxAge: 1 * 24 * 60 * 60 * 1000,
    secure: false,
  },
};
