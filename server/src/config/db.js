const url = process.env.MONGO_URL || 'mongodb://localhost';

export default {
  url,
  options: {
    dbName: 'chatapp',
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    autoIndex: false,
    bufferCommands: false,
    poolSize: 5,
  }
};
