import winston from 'winston';
import rootPath from 'app-root-path';

const isProduction = () => process.env.NODE_ENV === 'production';
const isTesting = () => process.env.NODE_ENV === 'test';

// NOTE: custom settings for each transport
const options = {
  file: {
    name: 'file',
    level: isProduction() ? 'info' : isTesting() ? [] : 'debug',
    filename: `${rootPath}/logs/chatapp.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    // need to rotate them
    maxFiles: 5,
    colorize: false,
  },
  // NOTE: docker may also need this for running `docker logs web`
  console: {
    name: 'console',
    level: isTesting() ? [] : 'info',
    handleExceptions: true,
    json: false,
    colorize: true,
  }
};

const logger = winston.createLogger({
  // NOTE: this is intentional, and since the app risks to become unstable,
  // nodemon (in dev) or docker (in prod) should take care of restarting
  exitOnError: true,
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ]
});

logger.stream = {
  write: function(message) {
    logger.info(message);
  },
};

export default logger;
