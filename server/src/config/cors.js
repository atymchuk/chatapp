const options = {
  origin: process.env.CLIENT_URL || 'http://localhost:3000',
  credentials: true,
};

export default options;
