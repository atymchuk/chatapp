import logger from '../config/logger';

const logError = err => logger.error(err);

export default function socketApi(app, socket) {
  const models = app.set('models');

  socket.on('connected', async ({ username }) => {
    const { Room, Message } = models;
    console.log('connected', username);
    try {
      const rooms = await Room.find({});
      socket.emit('rooms', rooms);
      // make sure we send the rooms first
      const messages = await Message.find({}).sort({ time: 1 });
      socket.emit('messages', messages);

    } catch (error) {
      logError(error);
    }
  });

  socket.on('message/new', async (message) => {
    const { Message } = models;

    try {
      await new Message(message).save();
      // send this message to all connected clients but the author
      socket.broadcast.emit('message/new', message);
      // confirm back to client that the message has been saved
      socket.emit('message/sent', message);

    } catch (error) {
      logError(error);
    }
  });

  socket.on('participant/join', async ({ roomId, userId }) => {
    const { User } = models;

    try {
      // update the user current room
      await User.update({ local: { username: userId }}, { $set: { currentRoomId: roomId } });
      // tell others to update their participant list
      socket.broadcast.emit('participant/joined', { roomId, userId });
      // get the participant list for this room
      const participants = await User.find({ currentRoomId: roomId })
        .then(users => users.map(user => user.local.username));
      // send the list to the user
      socket.emit('participants/push', { roomId, participants });

    } catch (error) {
      logError(error);
    }
  });

  socket.on('participant/leave', async ({ roomId, userId }) => {
    const { User } = models;
    try {
      // update the user's current room
      await User.update({ local: { username: userId }}, { $set: { currentRoomId: null } });
      socket.broadcast.emit('participant/left', { roomId, userId });
    } catch (error) {
      logError(error);
    }
  });

  socket.on('new room', async (name) => {
    const { Room } = models;

    try {
      const { _doc: room } = await new Room({ name, private: false }).save();

      socket.broadcast.emit('new room', room);
      // confirm back to client that the room has been saved
      socket.emit('new room', room);

    } catch (error) {
      // tell the user there was a problem
      socket.emit('new room error', `Error: couldn't create a new room ${name}`);
      logError(error);
    }
  });

  socket.on('disconnect', (reason) => {
    socket.broadcast.emit('disconnected', reason);
  });

}
