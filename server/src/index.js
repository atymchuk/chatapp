import { Server } from 'http';
import express from 'express';
import socketIO from 'socket.io';
import morgan from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import connectMongo from 'connect-mongo';
import session from 'express-session';
import compression from 'compression';

import api from './api';
import models from './models';
import socketAPI from './socket';

import dbConfig from './config/db';
import corsConfig from './config/cors';
import sessionConfig from './config/session';
import logger from './config/logger';

mongoose.connect(dbConfig.url, dbConfig.options, (err) => {
  if (err) {
    logger.error(err.toString());
    process.exit(1);
  }
});

const port = process.env.PORT || 3333;

const app = express();
const server = Server(app);
const io = socketIO.listen(server, { cookie: false });

app.enable('trust proxy');
app.disable('x-powered-by');
app.disable('etag');

app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(morgan('combined', {
  stream: logger.stream,
  // skip pre-flight requests when logging
  skip: function (req, res) {
    return req.method === 'OPTIONS';
  }
}));

app.set('mongoose', mongoose);
app.set('models', models);

const MongoStore = connectMongo(session);
const sessionMiddleware = session({
  ...sessionConfig,
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
    ttl: sessionConfig.cookie.ttl / 1000,
  })
});

io.use(function(socket, next) {
  // share the cookie between the socket & the session
  sessionMiddleware(socket.request, socket.request.res, next);
});

app.use(sessionMiddleware);

// NOTE: the origin should be changed for prod
app.use(cors(corsConfig));

// launch api middleware
api(app);

// connect the socket API
io.on('connection', function(socket) {
  app.set('socket', socket);
  socketAPI(app, socket);
});

// error handling
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);
app.use(notFoundHandler);

function logErrors(err, req, res, next) {
  logger.error(err.stack);
  next(err);
}

function clientErrorHandler(err, req, res, next) {
  if (req.xhr) {
    logger.error(err.toString());
    res.status(500).json({ error: `Unhandled server error.` });
  } else {
    next(err);
  }
}

function errorHandler(err, req, res, next) {
  logger.error(err.toString());
  res.status(500).json({ error: err });
}

function notFoundHandler(req, res, next) {
  const msg = `Error: no route handler for '${req.url}'\n`;
  logger.error(msg);
  res.status(404).send('Not found');
}

function shutdownMongo() {
  mongoose.connection.close(function () {
    logger.info('Mongoose connection is closed through app termination');
    process.exit(0);
  });
}

// if the Node process ends, close the mongoose connection
process
  .on('SIGINT', shutdownMongo)
  .on('SIGTERM', shutdownMongo);

server.listen(port, (error) => {
  if (error) {
    throw error;
  } else {
    logger.info(`Server listening on port ${port}`);
  }
});

export default app;

export { server };
