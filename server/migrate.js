import mongoose from 'mongoose';
import fs from 'fs';
import path from 'path';

import config from './src/config/db';

const migrationsOrder = ['users', 'rooms', 'messages'];

mongoose.connect(config.url, config.options, async (error) => {
  if (error) {
    console.error(error.toString());
    process.exit(1);
  }

  const args = process.argv.slice(2);
  const direction = args[0];
  let successful = 0;

  if (!['up', 'down'].includes(direction)) {
    console.log(`The migration direction should be either 'up' or 'down'`);
    return close();
  }

  const files = fs.readdirSync('./migrations')
    .filter(file => /\.js$/.test(file))
    .map(file => file.replace(/\.js$/, ''));

  if (!files.length) {
    console.log('No migrations to run');
    return close();
  }

  const migrationFiles = migrationsOrder.filter(file => files.includes(file));

  for (const file of migrationFiles) {
    const script = require(path.resolve(__dirname, 'migrations', file));
    console.log(`Running migration: ${file}`);
    await script[direction]()
      .then(() => {
          successful++;
          console.log(`Migration '${file}' was successful`);
        })
        .catch(error => {
          console.log(`Migration '${file}' failed with error: ${error.toString()}`);
        });
  }

  if (files.length === successful) {
    console.log('Summary: all migrations ran successfully');
  } else {
    console.log('Summary: some migrations failed');
  } 

  close();
});

function close() {
  // close the connection & exit
  mongoose.connection.close(() => process.exit(0));
}
