import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { compose, combineReducers, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { loggers } from 'redux-act';

import './index.css';
import reducers from './reducers';
import { App } from './components/app';

import * as serviceWorker from './serviceWorker';

const isDevelopment = process.env.NODE_ENV === 'development';
const loggerMiddleware = createLogger({ ...loggers.reduxLogger, collapsed: true });
const middlewares = [thunkMiddleware, isDevelopment && loggerMiddleware];

const composedStore = compose(applyMiddleware(...middlewares.filter(Boolean)));
const storeCreator = composedStore(createStore);
const store = storeCreator(
  combineReducers(reducers),
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
