import io from 'socket.io-client';

const socketUrl = 'http://localhost:3333';

const socket = io(socketUrl);

export default socket;
