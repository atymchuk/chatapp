import { createAction } from 'redux-act';

export const addParticipant = createAction('ADD_PARTICIPANT');

export const removeParticipant = createAction('REMOVE_PARTICIPANT');

export const updateParticipants = createAction('UPDATE_PARTICIPANTS');

