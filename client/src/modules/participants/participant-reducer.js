import { createReducer } from 'redux-act';
import { addParticipant, removeParticipant, updateParticipants } from './participant-actions';
import { logoutSuccess } from '../auth';

const defaultState = {};
/**
 The state shape here is:
  {
    [roomId]: [
      'Participant 1',
      'Participant ...',
      'Participant 100',
    ],
    [roomId]: [...]
  }
 */

export const participantReducer = createReducer(
  {
    [addParticipant]: (state, { roomId, userId }) => {
      const room = state[roomId] || [];
      return !room.includes(userId)
        ? {
          ...state,
          [roomId]: room.concat(userId).sort(),
        }
        : state;
    },

    [removeParticipant]: (state, { roomId, userId }) => {
      const room = state[roomId] || [];
      return room.includes(userId)
        ? {
          ...state,
          [roomId]: room.filter(username => username !== userId).sort(),
        }
        : state;
    },

    [updateParticipants]: (state, { roomId, participants }) => {
      return {
        ...state,
        [roomId]: participants.sort(),
      };
    },

    [logoutSuccess]: (state) => {
      return defaultState;
    }
  },
  defaultState
);
