import puppeteer from 'puppeteer';

describe('Auth tests', () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch({
      headless: true
    });

    page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 1440,
        height: 900
      },
      userAgent: ''
    });
  });

  test('user can login', async () => {
    await page.goto('http://localhost:3000');

    await page.waitForSelector('#username');
    await page.click('input[id=username]');
    await page.type('input[id=username]', 'johndoe');

    await page.click('button[data-testid=login-submit]');

    await page.waitForSelector('[data-testid=room-component]');

  }, 5000);

  test('user can see his/her name on the toolbar', async () => {

    await page.waitForSelector('[data-testid=room-component-username]');

    const username = await page.$eval('[data-testid=room-component-username]', e => e.innerHTML);

    expect(username).toEqual('johndoe');

  }, 5000);

  test('participant counter show the actual number of connected people', async () => {
    await page.waitForSelector('[data-testid=participant-counter]');
    const count = await page.$eval('[data-testid=participant-counter]', e => e.innerHTML);

    page.waitFor(500);
    expect(count).toEqual('1');

  }, 5000);

  test('user can see a list of connected people', async () => {
    await page.waitForSelector('[data-testid=show-participants-button]');
    await page.click('[data-testid=show-participants-button]');
    await page.waitForSelector('#form-dialog-title');

    const title = await page.$eval('#form-dialog-title > h2', e => e.innerHTML);

    expect(title).toEqual('Chat Participants');

    await page.waitForSelector('[data-testid=participants-dialog-close-button]');
    await page.click('[data-testid=participants-dialog-close-button]');

  }, 5000);

  test('user can logout', async () => {
    // wait a second to let the dialog animation finish
    await page.waitFor(1000);
    await page.waitForSelector('[data-testid=logout-button]');
    await page.click('[data-testid=logout-button]');

    await page.waitForSelector('#username');

  }, 5000);

  afterAll(() => {
    browser.close();
  });
});
