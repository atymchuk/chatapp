import { createSelector } from 'reselect';

const getAuthState = state => state.auth;

export const getIsAuthenticated = createSelector(
  getAuthState,
  auth => auth.isAuthenticated,
);

export const getIsPending = createSelector(
  getAuthState,
  auth => auth.pending,
);
