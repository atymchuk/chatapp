import { createReducer } from 'redux-act';

import {
  loginStart,
  loginSuccess,
  loginError,
  logoutSuccess,
  pingStart,
  pingSuccess,
  pingError,
} from './auth-actions';

const defaultState = {
  isAuthenticated: false,
  username: null,
  pending: false,
};

export const authReducer = createReducer(
  {
    [loginError]: () => {
      return { ...defaultState, pending: false };
    },

    [loginStart]: state => {
      return {
        ...state,
        pending: true,
      };
    },

    [loginSuccess]: (state, username) => {
      return {
        username,
        isAuthenticated: true,
        pending: false,
      };
    },

    [pingStart]: state => {
      return {
        ...state,
        pending: true,
      };
    },

    [pingSuccess]: (state, username) => {
      return {
        username,
        isAuthenticated: true,
        pending: false,
      };
    },

    [pingError]: () => {
      return { ...defaultState };
    },

    [logoutSuccess]: () => {
      return { ...defaultState };
    },
  },

  defaultState
);
