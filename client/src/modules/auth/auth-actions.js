import { createAction } from 'redux-act';
import axios from 'axios';

const rootUrl = 'http://localhost:3333/api/v1';
const userKey = 'chatapp.username';

export const loginStart = createAction('LOGIN_START');
export const loginSuccess = createAction('LOGIN_SUCCESS');
export const loginError = createAction('LOGIN_ERROR');

export function login(username) {
  return function(dispatch) {
    dispatch(loginStart());
    return axios({
      url: `${rootUrl}/auth/login`, 
      method: 'post',
      withCredentials: true,
      data: { username },
    })
    .then(res => res.data)
    .then(({ data: username }) => {
      localStorage.setItem(userKey, username);
      return dispatch(loginSuccess(username));
    })
    .catch(error => {
      localStorage.removeItem(userKey);
      dispatch(loginError());
    });
  }
}

export const logoutStart = createAction('LOGOUT_START');
export const logoutError= createAction('LOGOUT_ERROR');
export const logoutSuccess = createAction('LOGOUT_SUCCESS');

export function logout(payload) {
  return (dispatch) => {
    dispatch(logoutStart());

    return axios({
      url: `${rootUrl}/auth/logout?username=${payload.userId}`, 
      method: 'get',
      withCredentials: true,
    })
    .then(() => {
      localStorage.removeItem(userKey);
      dispatch(logoutSuccess());
    })
    .catch(error => {
      localStorage.removeItem(userKey);
      dispatch(logoutError());
    });
  }
}

export const pingStart = createAction('PING_START');
export const pingError= createAction('PING_ERROR');
export const pingSuccess = createAction('PING_SUCCESS');

export function ping() {
  const username = localStorage.getItem(userKey);

  return (dispatch) => {
    if (!username) {
      return Promise.resolve();
    }  

    dispatch(pingStart());

    return axios({
      url: `${rootUrl}/auth/ping`, 
      method: 'get',
      withCredentials: true,
    })
    .then(() => {
      return dispatch(pingSuccess(username));
    })
    .catch(error => {
      localStorage.removeItem(userKey);
      dispatch(pingError());
    });
  }
}
