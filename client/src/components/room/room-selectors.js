import { createSelector } from 'reselect';

const getAuthState = state => state.auth;
const getRoomState = state => state.rooms;
const getMessageState = state => state.messages;

export const getParticipants = state => state.participants;

export const getUsername = createSelector(
  getAuthState,
  auth => auth.username,
);

export const getActiveRoomId = createSelector(
  getRoomState,
  rooms => rooms.activeRoomId,
);

export const getRooms = createSelector(
  getRoomState,
  rooms => rooms.rooms,
);


export const getActiveRoomParticipants = createSelector(
  [getParticipants, getActiveRoomId],
  (participants, activeRoomId) => participants[activeRoomId] || [],
);

export const getActiveRoomMessages = createSelector(
  [getMessageState, getActiveRoomId],
  (messages, activeRoomId) => messages[activeRoomId] || [],
);
