import { createReducer } from 'redux-act';
import { createRoomSuccess, updateRooms, setActiveRoom } from './room-actions';
import { logoutSuccess } from '../../modules/auth';

const defaultState = {
  activeRoomId: null,
  rooms: [],
};

export const roomReducer = createReducer(
  {
    [createRoomSuccess]: (state, room) => {
      return {
        ...state,
        rooms: state.rooms.concat(room),
      };
    },

    [updateRooms]: (state, rooms) => {
      return {
        ...state,
        rooms,
      };
    },

    [setActiveRoom]: (state, roomId) => {
      return state.activeRoomId !== roomId
        ? {
          ...state,
          activeRoomId: roomId,
        }
        : state;
    },

    [logoutSuccess]: (state) => {
      return {
        ...defaultState
      };
    },
  },

  defaultState,
);
