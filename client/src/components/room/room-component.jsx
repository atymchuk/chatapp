import React, { PureComponent } from 'react';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import socket from '../../socket';
import { Message } from '../message';

import styles from './room.module.scss';

export class RoomComponent extends PureComponent {
  state = {
    message: '',
  };

  componentDidMount() {

    if (socket.connected) {
      socket.emit('connected', {
        username: this.props.username,
      });
    }

    socket.on('connect', () => {
      // notify the server in order to load the rooms & messages
      socket.emit('connected', {
        username: this.props.username,
      });
    });

    socket.on('disconnect', () => {
      console.log('disconnected');
    });

    // the list of rooms received after login or ping
    socket.on('rooms', rooms => {
      if (!socket.connected) {
        return;
      }
      this.props.updateRooms(rooms);
      if (!this.props.activeRoomId && rooms.length) {
        this.props.setActiveRoom(rooms[0].id);
        socket.emit('participant/join', { userId: this.props.username, roomId: this.props.activeRoomId });
      }
    });

    socket.on('participants/push', (payload) => {
      this.props.updateParticipants(payload);
    });

    socket.on('participant/joined', (payload) => {
      this.props.addParticipant(payload);
    });

    socket.on('participant/left', (payload) => {
      this.props.removeParticipant(payload);
    });

    // all messages received after login or ping
    socket.on('messages', messages => {
      if (!socket.connected) {
        return;
      }
      this.props.updateMessages(messages);
      this.scrollContentDown();
    });

    // own message sent and confirmed by the server
    socket.on('message/sent', message => {
      this.props.receiveMessage({
        roomId: this.props.activeRoomId,
        message,
      });
      this.setState({ message: '' }, () => {
        this.scrollContentDown();
      });
    });

    // other participant's message received
    socket.on('message/new', message => {
      this.props.receiveMessage({
        roomId: this.props.activeRoomId,
        message,
      });
      this.setState({ message: '' }, () => {
        this.scrollContentDown();
      });
    });
  }

  scrollContentDown() {
    const content = this.content;
    if (content) {
      content.scrollTop = content.scrollHeight - content.clientHeight;
    }
  }

  onMenu = () => {
    const payload = { userId: this.props.username, roomId: this.props.activeRoomId };
    socket.emit('participant/leave', payload);
    this.props.logout(payload);
  };

  onChange = ({ target }) => {
    this.setState({ message: target.value });
  };

  onSubmit = () => {
    const { activeRoomId: roomId, username: userId, sendMessage } = this.props;
    const message = { roomId, userId, text: this.state.message, time: new Date() };
    socket.emit('message/new', message);
    sendMessage(message);
  };

  render() {
    const { messages, username } = this.props;
    return (
      <main className={styles.Room} data-testid="room-component">
        <nav className={styles.MenuContainer}>
          <div className={styles.Menu} data-testid="room-component-username">
            {this.props.username}
          </div>
          <div className={styles.MenuButton} onClick={this.onMenu} data-testid="logout-button">
            <ExitToAppIcon />
          </div>
        </nav>
        <section className={styles.ContentWrapper}>
          <div className={styles.Content} ref={(el) => { this.content = el; }}>
            {messages.map((msg, i) => <Message key={i} username={username} {...msg} />)}
          </div>
          <div className={styles.InputBox}>
            <textarea
              name="message"
              className={styles.Input}
              rows="2"
              value={this.state.message}
              onChange={this.onChange}
              placeholder="Type your message"
              data-testid="message-input"
            />
          </div>
          <button
            type="button"
            className={styles.Submit}
            onClick={this.onSubmit}
            disabled={!this.state.message}
            data-testid="message-submit-button"
          >
          Submit
          </button>
        </section>
      </main>
    );
  }
}
