import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { RoomComponent } from './room-component';
import { logout } from '../../modules/auth';
import { updateRooms, setActiveRoom } from './room-actions';
import { sendMessage, receiveMessage, updateMessages } from '../message';
import { updateParticipants, addParticipant, removeParticipant } from '../../modules/participants';
import {
  getActiveRoomMessages,
  getUsername,
  getActiveRoomId,
  getRooms,
} from './room-selectors';

const mapStateToProps = createStructuredSelector({
  username: getUsername,
  activeRoomId: getActiveRoomId,
  rooms: getRooms,
  messages: getActiveRoomMessages,
});

const mapDispatchToProps = dispatch => ({
  setActiveRoom: roomId => dispatch(setActiveRoom(roomId)),
  logout: name => dispatch(logout(name)),
  updateRooms: rooms => dispatch(updateRooms(rooms)),
  // these two serve the logging purpose
  updateMessages: messages => dispatch(updateMessages(messages)),
  sendMessage: message => dispatch(sendMessage(message)),
  receiveMessage: message => dispatch(receiveMessage(message)),
  updateParticipants: payload => dispatch(updateParticipants(payload)),
  addParticipant: participant =>  dispatch(addParticipant(participant)),
  removeParticipant: participant =>  dispatch(removeParticipant(participant)),
});

export const Room = connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomComponent);
