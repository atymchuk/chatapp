import { createAction } from 'redux-act';

export const createRoomStart = createAction('CREATE_ROOM_START');

export const createRoomSuccess = createAction('CREATE_ROOM_SUCCESS');

export const createRoomError = createAction('CREATE_ROOM_ERROR');

export const updateRooms = createAction('UPDATE_ROOMS');

export const setActiveRoom = createAction('SET_ACTIVE_ROOM');
