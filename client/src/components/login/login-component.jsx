import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';

const styles = theme => ({
  loginContainer: {
    position: 'relative',
    width: 350,
    height: '100vh',
    margin: '0 auto',
  },
  loginForm: {
    position: 'absolute',
    padding: '30px',
    width: '100%',
    backgroundColor: '#ece6fb',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    boxShadow: '0 1px 12px 3px rgba(140, 140, 140, 0.1)'
  },
  submit: {
    marginTop: '2em',
  },
  headline: {
    marginBottom: '1em',
  }
});

class Login extends Component {
  state = {
    name: ''
  };

  onChange = ({ target }) => {
    this.setState({
      name: target.value,
    });
  }

  onKeyPress = ({ key }) => {
    if (key === 'Enter' && this.state.name) {
      this.onSubmit();
    }
  }

  onSubmit = () => {
    this.props.login(this.state.name);
  }

  render() {
    const { classes } = this.props;
    return (
      <section className={classes.loginContainer}>
        <div className={classes.loginForm}>
          <Typography className={classes.headline} variant="headline" component="h2">
            Chat App
          </Typography>
          <FormControl fullWidth>
            <InputLabel htmlFor="username">Nickname</InputLabel>
            <Input
              id="username"
              autoComplete="off"
              value={this.state.name}
              startAdornment={
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              }
              onChange={this.onChange}
              onKeyPress={this.onKeyPress}
            />
          </FormControl>
          <Button
            onClick={this.onSubmit}
            className={classes.submit}
            variant="contained"
            color="default"
            fullWidth
            disabled={!this.state.name}
            data-testid="login-submit"
          >
            Login
          </Button>
        </div>
      </section>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export const LoginComponent = withStyles(styles)(Login);
