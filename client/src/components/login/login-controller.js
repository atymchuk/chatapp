import { connect } from 'react-redux';

import { LoginComponent } from './login-component';
import { login } from '../../modules/auth';

const mapDispatchToProps = dispatch => ({
  login: (name) => dispatch(login(name))
});

export const Login = connect(null, mapDispatchToProps)(LoginComponent);
