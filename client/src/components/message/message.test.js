import puppeteer from 'puppeteer';

describe('Message tests', () => {
  let browser;
  let page;
  const messageText = 'Hello everybody!';

  beforeAll(async () => {
    browser = await puppeteer.launch({
      headless: true
    });

    page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 1440,
        height: 900
      },
      userAgent: ''
    });

    await page.goto('http://localhost:3000');
  });

  test('user can post a message', async () => {
    // login
    await page.waitForSelector('#username');
    await page.click('input[id=username]');
    await page.type('input[id=username]', 'johndoe');
    await page.click('button[data-testid=login-submit]');
    await page.waitForSelector('[data-testid=room-component]');

    await page.waitForSelector('[data-testid=message-input]');
    await page.click('[data-testid=message-input]');
    await page.type('textarea[data-testid=message-input]', messageText);

    let message = await page.$eval('[data-testid=message-input]', e => e.innerHTML);

    expect(message).toEqual(messageText);

    await page.click('[data-testid=message-submit-button]');

    // need a delay to wait for the BE socket response
    page.waitFor(1000);

    function getLastElementText(nodes) {
      return nodes && nodes.length ? nodes[nodes.length - 1].innerHTML : '';
    }
    message = await page.$$eval('[data-testid=message-text]', getLastElementText);

    expect(message).toEqual(messageText);

  }, 5000);

  test('user can see other users\' messages', async () => {

    // login
    await page.waitForSelector('#username');
    await page.click('input[id=username]');
    await page.type('input[id=username]', 'larryking');
    await page.click('button[data-testid=login-submit]');
    await page.waitForSelector('[data-testid=message-text]');

    function getLastElementText(nodes) {
      return nodes && nodes.length ? nodes[nodes.length - 1].innerHTML : '';
    }
    const message = await page.$$eval('[data-testid=message-text]', getLastElementText);

    // this was the message from another user
    expect(message).toEqual(messageText);

  }, 5000);

  afterEach(async () =>{
    // logout
    await page.waitForSelector('[data-testid=logout-button]');
    await page.click('[data-testid=logout-button]');
    await page.waitForSelector('#username');
  });

  afterAll(async () => {
    browser.close();
  });
});
