import React from 'react';
import cn from 'classnames';
import moment  from 'moment';

import styles from './message.module.scss';

export const Message = ({ userId, time, text, username }) => {
  const dateTime = moment(time).format('HH:mm DD.MM.YYYY');
  const isAuthor = userId === username;
  return (
    <div className={styles.MessageWrapper}>
      <div className={cn(styles.Message, {[styles.Right]: isAuthor })}>
        <div className={styles.Header}>
          <div className={cn(styles.User, {[styles.Author]: isAuthor })}>
            {isAuthor ? 'You' : userId}
          </div>
          <div className={styles.Time}>{dateTime}</div>
        </div>
        <div className={styles.Text} data-testid="message-text">{text}</div>
      </div>
    </div>
  );
};
