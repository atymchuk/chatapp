import { createReducer } from 'redux-act';
import { receiveMessage, updateMessages } from './message-actions';
import { updateRooms } from '../room';
import { logoutSuccess } from '../../modules/auth';

const defaultState = {};
/**
 The state shape here is:
  {
    [roomId]: [
      {  Message 1 },
      {  Message ... },
      {  Message 100 },
    ],
    [roomId]: [...]
  }
 */

 /**
  * Note: when the rooms update, we need to add a roomId to this state,
  * since all messages are stored with a roomId key
  */
export const messageReducer = createReducer(
  {
    [updateRooms]: (state, rooms) => {
      return rooms.reduce((acc, { id: roomId }) => {
        return acc[roomId]
          ? acc
          : { ...acc, [roomId]: [] };
      }, state);
    },

    [receiveMessage]: (state, { roomId, message }) => {
      return {
        ...state,
        [roomId]: state[roomId]
          ? state[roomId].concat(message)
          : [message],
      };
    },

    [updateMessages]: (state, messages) => {
      return messages.reduce((acc, message) => {
        if (acc[message.roomId]) {
          acc[message.roomId].push(message);
          return acc;
        }
        acc[message.roomId] = [message];
        return acc;
      }, {});
    },

    [logoutSuccess]: (state) => {
      return {
        ...defaultState
      };
    },
  },
  defaultState,
);
