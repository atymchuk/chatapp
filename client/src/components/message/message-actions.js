import { createAction } from 'redux-act';
export const sendMessageStart = createAction('SEND_MESSAGE_START');

export const sendMessageSuccess = createAction('SEND_MESSAGE_SUCCESS');

export const sendMessageError = createAction('SEND_MESSAGE_ERROR');

export const updateMessages = createAction('UPDATE_MESSAGES');

export const sendMessage = createAction('SEND_MESSAGE');

export const receiveMessage = createAction('RECEIVE_MESSAGE');
