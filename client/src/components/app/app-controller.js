import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { AppComponent } from './app-component';
import { login, ping } from '../../modules/auth';
import { getIsAuthenticated, getIsPending } from '../../modules/auth';

const mapStateToProps = createStructuredSelector({
  isAuthenticated: getIsAuthenticated,
  pending: getIsPending,
 });

const mapDispatchToProps = dispatch => ({
  login: (name) => dispatch(login(name)),
  ping: () => dispatch(ping()),
});

export const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent);
