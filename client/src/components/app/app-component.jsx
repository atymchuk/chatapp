import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';

import { Sidebar } from '../sidebar';
import { Login } from '../login';
import { Room } from '../room';

import styles from './app.module.scss';

export class AppComponent extends Component {

  componentDidMount() {
    this.props.ping();
  }

  render() {
    return this.props.isAuthenticated
      ? (
        <section className={styles.App}>
          <Sidebar />
          <Room />
        </section>
      )
      : !this.props.pending
        ? <Login />
        : (
          <div className={styles.LoaderWrapper}>
            <CircularProgress size={50} style={{ color: purple[500] }} />
          </div>
        );
  }
}
