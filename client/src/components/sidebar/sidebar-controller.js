import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { SidebarComponent } from './sidebar-component';
import {
  setActiveRoom,
  createRoomSuccess,
  createRoomStart,
  getActiveRoomId,
  getRooms,
  getParticipants,
  getActiveRoomParticipants,
} from '../room';

const mapStateToProps = createStructuredSelector({
  activeRoomId: getActiveRoomId,
  rooms: getRooms,
  roomParticipants: getActiveRoomParticipants,
  participants: getParticipants,
});

const mapDispatchToProps = dispatch => ({
  setActiveRoom: roomId => dispatch(setActiveRoom(roomId)),
  createRoomStart: name => dispatch(createRoomStart(name)),
  createRoomSuccess: room => dispatch(createRoomSuccess(room)),
});

export const Sidebar = connect(mapStateToProps, mapDispatchToProps)(SidebarComponent);
