import React, { Component } from 'react';
import cn from 'classnames';

import socket from '../../socket';

import { CreateRoomDialog } from './create-room-dialog';
import { ParticipantDialog } from './participant-dialog';

import styles from './sidebar.module.scss';

export class SidebarComponent extends Component {
  state = {
    showCreateDialog: false,
    showParticipantsDialog: false,
  };

  onClick = activeRoomId => () => {
    if (this.props.activeRoomId !== activeRoomId) {
      this.props.setActiveRoom(activeRoomId);
    }
  }

  onOpenCreateDialog = () => {
    this.setState({ showCreateDialog: true });
  }

  onCloseCreateDialog = () => {
    this.setState({ showCreateDialog: false });
  }

  onSubmit = name => {
    this.onCloseCreateDialog();
    socket.emit('new room', name);
    this.props.createRoomStart(name);
  }

  onOpenParticipantsDialog = (count) => () => {
    this.setState({ showParticipantsDialog: count > 0 });
  }

  onCloseParticipantsDialog = () => {
    this.setState({ showParticipantsDialog: false });
  }

  componentDidMount() {
    socket.on('new room', room => {
      const { _id, ...rest } = room;
      this.props.createRoomSuccess({ ...rest, id: _id });
    });
  }

  componentWillUnmount() {
    socket.off('new room');
  }

  render() {
    const { activeRoomId, participants, rooms, roomParticipants } = this.props;
    const activeRoomParticipantCount = (participants[activeRoomId] || []).length;

    return (
      <div className={styles.Sidebar}>
        <div className={styles.LogoContainer}>
          <div className={styles.Logo}>ChatApp</div>
          <div className={styles.CreateRoom} onClick={this.onOpenCreateDialog}>
            +
          </div>
        </div>
        <div className={styles.RoomContainer}>
          {rooms.map((room, i) => {
            const count = (participants[room.id] || []).length;
            return (
              <div
                key={i}
                className={cn(styles.RoomTile, {[styles.RoomTileActive]: room.id === activeRoomId})}
                onClick={this.onClick(room.id)}
              >
                <div className={styles.Room}>{room.name}</div>
                <div
                  className={cn(styles.Count, {[styles.Clickable]: !!count })}
                  onClick={this.onOpenParticipantsDialog(count)}
                  data-testid="show-participants-button"
                >
                  <span className={styles.Counter} data-testid="participant-counter">
                    {count}
                  </span>
                </div>
              </div>);
          })}
        </div>
        <CreateRoomDialog
          open={this.state.showCreateDialog}
          onClose={this.onCloseCreateDialog}
          onSubmit={this.onSubmit}
        />
        <ParticipantDialog
          participants={roomParticipants}
          open={this.state.showParticipantsDialog && !!activeRoomParticipantCount}
          onClose={this.onCloseParticipantsDialog}
        />
      </div>
    );
  }
}
