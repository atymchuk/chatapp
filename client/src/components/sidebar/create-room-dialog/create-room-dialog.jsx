import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 400,
    backgroundColor: theme.palette.background.paper,
  },
  dialog: {
    width: '80%',
  },
});

class CreateRoomDialogComponent extends Component {
  state = {
    room: ''
  };

  onChange = ({ target }) => {
    this.setState({ room: target.value });
  }

  onSubmit = () => {
    this.props.onSubmit(this.state.room);
    this.setState({ room: '' });
  }

  render() {
    const { classes, onClose, onSubmit, ...rest } = this.props;
    return (
      <div className={classes.root}>
        <Dialog
          {...rest}
          classes={{
            paper: classes.dialog,
          }}
          onClose={this.props.onClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Create Room</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="room"
              label="Room"
              type="text"
              fullWidth
              value={this.state.room}
              onChange={this.onChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="default">
              Cancel
            </Button>
            <Button onClick={this.onSubmit} color="primary" disabled={!this.state.room}>
              Create
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

CreateRoomDialogComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export const CreateRoomDialog = withStyles(styles)(CreateRoomDialogComponent);
