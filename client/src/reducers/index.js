import { authReducer } from '../modules/auth';
import { messageReducer } from '../components/message';
import { participantReducer } from '../modules/participants';
import { roomReducer } from '../components/room';

export default {
  auth: authReducer,
  messages: messageReducer,
  participants: participantReducer,
  rooms: roomReducer,
};
